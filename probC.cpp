/**
 * Author: Colin Burdine
 * Problem: sudokunique
 */

#include <iostream>
#include <map>
#include <vector>

using namespace std;

bool isValidRow(int** board, int row){
    bool check[9];
    int checkInd;
    for(int i =0 ; i < 9;++i){ check[i] = false; }
    for(int c = 0; c < 9; ++c){
        checkInd = board[row][c]-1;
        if(check[checkInd])
            return false;
        check[checkInd] = true;
    }
    return true;
}

bool validBlockRow(int** board, int bRow){
    bool check[9];
    int checkInd;
    for(int bCol = 0; bCol < 3; ++bCol){
        for(int i =0 ; i < 9;++i){ check[i] = false; }
        for(int i = 0; i < 9; ++i){
            checkInd = board[3*bRow + i/3][3*bCol + i%3]-1;
            if(check[checkInd])
                return false;
            check[checkInd] = true;
        }
    }
    return true;
}

int main() {

    int** board = new int*[9];
    for(int i = 0; i < 9; ++i){
        board[i] = new int [9];
    }
    vector<pair<int, int>> unknowns;
    vector<int> unknownsSoFar(9, 0);
    map<pair<int, int>, vector<bool> > possibleValues;
    string line;

    // read board:
    for(int r = 0; r < 9; ++r){
        if(r != 0){unknownsSoFar[r] = unknownsSoFar[r-1]; }
        for(int c = 0; c < 9; ++c){
            cin >> board[r][c];
            if(board[r][c] == 0){
                unknownsSoFar[r]++;
                unknowns.emplace_back(make_pair(r,c));
                possibleValues[make_pair(r,c)] = vector<bool>(9, true);
            }
        }
    }

    // generate candidate values in each box:
    vector<pair<int, int> > localUnknowns;

    //eliminate candidates in box:
    for(int boxR = 0; boxR < 3; ++boxR){
    for(int boxC = 0; boxC < 3; ++boxC){
        localUnknowns.clear();
        vector<bool> boxVals(9, true);
        for(int i = 0; i < 3; ++i){
        for(int j = 0; j < 3; ++j){
            int num = board[3*boxR+i][3*boxC+j];
            if(num == 0){
                localUnknowns.emplace_back(make_pair(3*boxR+i,3*boxC+j));
            }else{
                boxVals[num-1] = false;
            }
        }
        }

        for(auto& p : localUnknowns){
            for(int i =0; i < 9; ++i){
                if(!boxVals[i]){
                    possibleValues[p][i] = false;
                }
            }
        }
    }
    }

    //eliminate candidates in rows:
    for(int r = 0; r < 9; ++r){
        localUnknowns.clear();
        vector<bool> rowVals(9, true);
        for(int c = 0; c < 9; ++c){
            int num = board[r][c];
            if(num == 0){
                localUnknowns.emplace_back(make_pair(r, c));
            }else{
                rowVals[num-1] = false;
            }
        }
        for(auto& p : localUnknowns){
            for(int i =0; i < 9; ++i){
                if(!rowVals[i]){
                    possibleValues[p][i] = false;
                }
            }
        }
    }

    //eliminate candidates in cols:
    for(int c = 0; c < 9; ++c){
        localUnknowns.clear();
        vector<bool> rowVals(9, true);
        for(int r = 0; r < 9; ++r){
            int num = board[r][c];
            if(num == 0){
                localUnknowns.emplace_back(make_pair(r, c));
            }else{
                rowVals[num-1] = false;
            }
        }
        for(auto& p : localUnknowns){
            for(int i =0; i < 9; ++i){
                if(!rowVals[i]){
                    possibleValues[p][i] = false;
                }
            }
        }
    }

    //generate reduced set of possible values:
    vector<vector<int>> reducedVals(unknowns.size());
    vector<int> testKey(unknowns.size(), 0);
    vector<int> testKeyMaxes(unknowns.size());
    for(int v = 0; v < unknowns.size(); ++v){
        reducedVals.emplace_back(vector<int>());
        for(int i =0 ; i < 9; ++i){
            if(possibleValues[unknowns[v]][i]){
                reducedVals[v].push_back(i);
            }
        }
        testKeyMaxes[v] = reducedVals[v].size()-1;
    }

    //enumerate solutions:
    int keyInd = 0;
    int rowInd = 0;
    while(rowInd < 9){

        int startInd = (rowInd == 0)? 0 : unknownsSoFar[rowInd-1];
        int endInd = unknownsSoFar[rowInd];

        //generate new assignment of testKey in new row:
        for(int i = startInd; i < endInd; ++i){
            auto& p = unknowns[i];
            board[p.first][p.second] = reducedVals[i][testKey[i]];
        }

        //test validity of row:
        if(isValidRow(board, rowInd)){
            rowInd++;
        }else{
            //advance key to next possibility:
            int changeInd = startInd;
            while(changeInd < endInd && testKey[changeInd] < testKeyMaxes[changeInd]){ ++changeInd; }
            if(changeInd == endInd){
                rowInd--;
            }
        }

    }

    for(int i = 0; i < 9; ++i){
        delete board[i];
    }
    delete [] board;

}

